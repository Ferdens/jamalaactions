////
////  ActionsTableTableViewController.swift
////  actionsJamala
////
////  Created by Anton on 29.12.16.
////  Copyright © 2016 Macbook. All rights reserved.
////
//
//import UIKit
//
//protocol ActionsTableTableViewControllerDelegate {
//    func sendDataToInfoController(data: ActionInfoData)
//}
//
//class ActionsTableTableViewController: UITableViewController {
//    
//    
//    var delegate:ActionsTableTableViewControllerDelegate?
//    var data = ActionInfoData()
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        
//        let insets = UIEdgeInsets.init(top: 20, left: 0, bottom: 0, right: 0)
//        self.tableView.contentInset = insets
//        self.tableView.scrollIndicatorInsets = insets
//        self.tableView.allowsSelectionDuringEditing = false
//        self.tableView.separatorColor = UIColor.gray
//        
//    }
//    
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
//    
//    // MARK: - Table view data source
//    
//    override func numberOfSections(in tableView: UITableView) -> Int {
//        // #warning Incomplete implementation, return the number of sections
//        return 1
//    }
//    
//    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        // #warning Incomplete implementation, return the number of rows
//        return 2
//    }
//    
//    
//    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "actionCell", for: indexPath) as! CustomCell
//        
//        cell.dateLabel.text = "19.04"
//        cell.placeInfo.text = "Оперы и балета театр"
//        
//        return cell
//    }
//    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "toTheInfoController" {
//            self.delegate = segue.destination as! ActionInfoTableViewTableViewController
//        }
//    }
//   
//    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        self.data.date = "19.04"
//        self.data.time = "18:30"
//               self.delegate!.sendDataToInfoController(data: data)
//    }
//}
