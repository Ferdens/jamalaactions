//
//  ActionsScreenViewController.swift
//  actionsJamala
//
//  Created by Anton on 06.01.17.
//  Copyright © 2017 Macbook. All rights reserved.
//

import UIKit
protocol ActionsScreenViewControllerDelegate {
    func sendDataToInfoController(data: ActionInfoData)
}
class ActionsScreenViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    @IBOutlet weak var topViewImage: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var topView  : UIView!
    var delegate                :ActionsScreenViewControllerDelegate?
    var data                    = ActionInfoData()
    var previousOffset          : CGFloat?
    var minPointForMove         : CGFloat?
    var refreshControl = UIRefreshControl.init()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.minPointForMove = -self.view.frame.height * 0.25
        self.previousOffset = 0
        self.tableView.refreshControl = self.refreshControl
        self.refreshControl.addTarget(self, action: #selector(ActionsScreenViewController.refreshing), for:
            .valueChanged)
        do {
//
           
            let data = try Data.init(contentsOf: URL.init(string:"https://pp.vk.me/c622917/v622917415/420a6/y1yc_gcT_4I.jpg")!)
            self.topViewImage.image = UIImage.init(data: data)
        } catch {
            print("da i huy s nim")
        }
        
        
    }
    //MARK: Refresh control
    func refreshing() -> Void {
        self.refreshControl.endRefreshing()
    }
    //MARK: Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toTheInfoController" {
            self.delegate = segue.destination as! ActionInfoTableViewController
        }
    }
    //MARK: Scroll animation
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if (scrollView.contentOffset.y <= self.view.frame.height * 0.35) && (scrollView.contentOffset.y > 0) {
            var rect = self.topView.frame
            rect.origin.y += previousOffset! - scrollView.contentOffset.y
            self.tableView.center.y += previousOffset! - scrollView.contentOffset.y
            self.previousOffset  = scrollView.contentOffset.y
            self.topView.frame   = rect
            
        }
        
    }
    
    
    // MARK: - Table view data source/delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 20
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "actionCell", for: indexPath) as! CustomCell
        cell.dateLabel.text = "19.04"
        cell.placeInfo.text = "Оперы и балета театр"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.data.date = "19.04"
        self.data.time = "18:30"
        self.delegate!.sendDataToInfoController(data: data)
    }
}
