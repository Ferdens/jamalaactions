//
//  ActionInfoData.swift
//  actionsJamala
//
//  Created by Anton on 05.01.17.
//  Copyright © 2017 Macbook. All rights reserved.
//

import Foundation


struct ActionInfoData {
    
    var date: String?
    var time: String?
    var whereICanBuyTickets :[String]?

}
