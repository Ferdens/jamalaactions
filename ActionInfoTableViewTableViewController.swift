//
//  ActionInfoTableViewController.swift
//  actionsJamala
//
//  Created by Anton on 05.01.17.
//  Copyright © 2017 Macbook. All rights reserved.
//

import UIKit

class ActionInfoTableViewController: UITableViewController,ActionsScreenViewControllerDelegate {
    
    @IBOutlet weak var actionImage: UIImageView!
    @IBOutlet weak var timeInfoLabel: UILabel!
    @IBOutlet weak var dateInfo: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let insets = UIEdgeInsets.init(top: 20, left: 0, bottom: 0, right: 0)
        self.tableView.contentInset = insets
        self.tableView.scrollIndicatorInsets = insets
        self.tableView.allowsSelectionDuringEditing = false
        self.tableView.separatorColor = UIColor.gray
        
//        let frame = CGRect.init(x: 0, y: 0, width: self.view.bounds.width, height: 100)
//        let actionsTopView = UIView.init(frame: frame)
//        actionsTopView.backgroundColor = UIColor.blue
//        self.view.addSubview(actionsTopView)

    }
    
    
    
   
    //MARK: Data recive
    func sendDataToInfoController(data: ActionInfoData) {
        self.timeInfoLabel.text = data.time ?? "nil"
        self.dateInfo.text      = data.date ?? "nil"
    }
  
    
    
}
